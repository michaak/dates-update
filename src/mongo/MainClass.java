/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;

/**
 *
 * @author acer
 */
public class MainClass {

    /**
     * @param args the command line arguments
     */
    private static DB db;

    private List<String> headerAttributes;

    public void doDatesUpdate(String server, String database, int port) {
        try {
            //Database connection
            MongoClient mongo = new MongoClient(server, port);
            db = mongo.getDB(database);
            System.out.println("Connection Ok!!!");
            DBCollection templates = db.getCollection("templates");

            DBCursor cursor = templates.find();
            System.out.println("Browsing templates... : Begin");
            while (cursor.hasNext()) {
                DBObject temp = cursor.next();
                String id = temp.get("_id").toString();
                headerAttributes = new ArrayList<>();
                DBObject feature = (DBObject) temp.get("feature");
                ArrayList temp_attributes = (ArrayList) feature.get("attributes");

                buildHeaderAttributes(temp_attributes, null);
                System.out.println("Updating features of template "+id+ " : Begin");
                updateFeatures(headerAttributes, id);
                System.out.println("Updating features of template "+id+ " : End");
            }
            System.out.println("Browsing templates... : End");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void doDatesUpdate2(String server, String database, int port) {
//        try {
//            //Database connection
//            MongoClient mongo = new MongoClient(server, port);
//            db = mongo.getDB(database);
//
//            DBCollection templates = db.getCollection("templates");
//
//            BasicDBObject searchQuery = new BasicDBObject().append("_id", new ObjectId("5a02d923c1cbcbaa400d62aa"));
//
//            DBCursor cursor = templates.find(searchQuery);
//
//            while (cursor.hasNext()) {
//                DBObject temp = cursor.next();
//                String id = temp.get("_id").toString();
//                headerAttributes = new ArrayList<>();
//                DBObject feature = (DBObject) temp.get("feature");
//                ArrayList temp_attributes = (ArrayList) feature.get("attributes");
//
//                buildHeaderAttributes(temp_attributes, null);
//
//                updateFeatures(headerAttributes);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * build path to all attributes which type is date
     *
     * @param attributes
     * @param prefix
     */
    public void buildHeaderAttributes(ArrayList attributes, String prefix) {
        String tempPrefix = "";
        for (int i = 0; i < attributes.size(); i++) {

            Object object = attributes.get(i);

            DBObject elmt = (DBObject) object;

            if (elmt.get("_class").toString().equalsIgnoreCase("attributeset")) {//attributeset

                ArrayList fs_attributes = (ArrayList) elmt.get("attributes");

                String fs_name = elmt.get("name").toString();

                if (prefix == null || "".equals(prefix)) {
                    tempPrefix = fs_name;
                } else {
                    tempPrefix = prefix + "." + fs_name;
                }

                buildHeaderAttributes(fs_attributes, tempPrefix);

            } else if (elmt.get("_class").toString().equalsIgnoreCase("attribute")) { //Attribute

                String type = elmt.get("type").toString();
                String f_name = elmt.get("name").toString();
                if (prefix == null || "".equals(prefix)) {
                    tempPrefix = f_name;
                } else {
                    tempPrefix = prefix + "." + f_name;
                }

                if (type.equalsIgnoreCase("date")) {
                    headerAttributes.add(tempPrefix);
                }

            }

        }

    }

    /**
     * Update all date attributes in a feature
     *
     * @param headerAttributes
     * @param temp_id
     */
    public void updateFeatures(List<String> headerAttributes, String temp_id) {

        try {

            DBCollection features = db.getCollection("features");

            BasicDBObject searchQuery2 = new BasicDBObject().append("template_id", temp_id);

            DBCursor cursor2 = features.find(searchQuery2);

            while (cursor2.hasNext()) {

                DBObject feat = cursor2.next();
                //System.out.println("feat = " + feat);
                //feature id
                String feature_id = feat.get("_id").toString();
                //attributes
                DBObject feat_attributes = (DBObject) feat.get("attributes");
                if (!headerAttributes.isEmpty()) {
                    //System.out.println("not empty");
                    for (String key : headerAttributes) {

                        DBObject pathtovalue = feat_attributes;

                        String keys[] = key.split("\\.");

                        for (int i = 0; i < keys.length; i++) {
                            pathtovalue = (DBObject) pathtovalue.get(keys[i]);
                        }

                        BasicDBObject updateDocument = new BasicDBObject();

                        //BasicDBObject userSets = new BasicDBObject();
                        BasicDBObject fieldSets = new BasicDBObject();

                        //Update the attribute field which type is date
                        Date date = null;
                        String datestr = null;

                        if (pathtovalue != null) {
                            Object valueobj = pathtovalue.get("value");

                            String typeString = valueobj == null ? "null" : valueobj.getClass().toString();

                            if (valueobj != null && !typeString.contains("java.util.Date")) {
                                //System.out.println("valueobj = " + valueobj);
                                datestr = valueobj.toString();
                                date = parseDate(datestr);
                                fieldSets.put("attributes." + key + ".value", date);
                                //updateDocument.append("$set", new BasicDBObject().append("attributes." + key + ".value", date));
                            }
                        }

                        //update created_at and updated_at
                        String screated_at = null, supdated_at = null, sdeleted_at = null;
                        Date created_at, updated_at, deleted_at;

                        Object created = feat.get("created_at");
                        String typecreated = created == null ? "null" : created.getClass().toString();

                        Object updated = feat.get("updated_at");
                        String typeupdated = updated == null ? "null" : updated.getClass().toString();

                        Object deleted = feat.get("deleted_at");
                        String typedeleted = deleted == null ? "null" : deleted.getClass().toString();

                        if (created != null && !typecreated.contains("java.util.Date")) {
                            screated_at = created.toString();
                            created_at = parseDate(screated_at);
                            fieldSets.put("created_at", created_at);
                            //updateDocument.append("$set", new BasicDBObject().append("created_at", created_at));
                        }

                        if (updated != null && !typeupdated.contains("java.util.Date")) {
                            supdated_at = updated.toString();
                            updated_at = parseDate(supdated_at);
                            fieldSets.put("updated_at", updated_at);
                            //updateDocument.append("$set", new BasicDBObject().append("updated_at", updated_at));
                        }

                        if (deleted != null && !typedeleted.contains("java.util.Date")) {
                            sdeleted_at = deleted.toString();
                            deleted_at = parseDate(sdeleted_at);
                            fieldSets.put("deleted_at", deleted_at);
                            //updateDocument.append("$set", new BasicDBObject().append("deleted_at", deleted_at));
                        }
                        updateDocument.put("$set", fieldSets);

                        BasicDBObject searchQueryUpdate = new BasicDBObject().append("_id", new ObjectId(feature_id));

                        if (!fieldSets.isEmpty()) {
                            features.update(searchQueryUpdate, updateDocument);
                        }

                    }
                } else {
                    BasicDBObject updateDocument = new BasicDBObject();
                    BasicDBObject fieldSets = new BasicDBObject();
                    //update created_at and updated_at
                    String screated_at = null, supdated_at = null, sdeleted_at = null;
                    Date created_at, updated_at, deleted_at;

                    Object created = feat.get("created_at");
                        String typecreated = created == null ? "null" : created.getClass().toString();

                        Object updated = feat.get("updated_at");
                        String typeupdated = updated == null ? "null" : updated.getClass().toString();

                        Object deleted = feat.get("deleted_at");
                        String typedeleted = deleted == null ? "null" : deleted.getClass().toString();

                    if (created != null && !typecreated.contains("java.util.Date")) {
                        screated_at = created.toString();
                        created_at = parseDate(screated_at);
                        fieldSets.put("created_at", created_at);
                        //updateDocument.append("$set", new BasicDBObject().append("created_at", created_at));
                    }

                    if (updated != null && !typeupdated.contains("java.util.Date")) {
                        supdated_at = updated.toString();
                        updated_at = parseDate(supdated_at);
                        fieldSets.put("updated_at", updated_at);
                        //updateDocument.append("$set", new BasicDBObject().append("updated_at", updated_at));
                    }

                    if (deleted != null && !typedeleted.contains("java.util.Date")) {
                        sdeleted_at = deleted.toString();
                        deleted_at = parseDate(sdeleted_at);
                        fieldSets.put("deleted_at", deleted_at);
                        //updateDocument.append("$set", new BasicDBObject().append("deleted_at", deleted_at));
                    }
                    updateDocument.put("$set", fieldSets);

                    BasicDBObject searchQueryUpdate = new BasicDBObject().append("_id", new ObjectId(feature_id));

                    if (!fieldSets.isEmpty()) {
                        features.update(searchQueryUpdate, updateDocument);
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateFeatures(List<String> headerAttributes) {

        try {

            DBCollection features = db.getCollection("features");

            BasicDBObject searchQuery2 = new BasicDBObject().append("template_id", "5a02d923c1cbcbaa400d62aa");

            DBCursor cursor2 = features.find(searchQuery2);

            while (cursor2.hasNext()) {

                DBObject feat = cursor2.next();
                //System.out.println("feat = " + feat);
                //feature id
                String feature_id = feat.get("_id").toString();
                //attributes
                DBObject feat_attributes = (DBObject) feat.get("attributes");
                if (!headerAttributes.isEmpty()) {
                    //System.out.println("not empty");
                    for (String key : headerAttributes) {

                        DBObject pathtovalue = feat_attributes;

                        String keys[] = key.split("\\.");

                        for (int i = 0; i < keys.length; i++) {
                            pathtovalue = (DBObject) pathtovalue.get(keys[i]);
                        }

                        BasicDBObject updateDocument = new BasicDBObject();

                        //BasicDBObject userSets = new BasicDBObject();
                        BasicDBObject fieldSets = new BasicDBObject();

                        //Update the attribute field which type is date
                        Date date = null;
                        String datestr = null;

                        if (pathtovalue != null) {
                            Object valueobj = pathtovalue.get("value");

                            String typeString = valueobj == null ? "null" : valueobj.getClass().toString();

                            if (valueobj != null && !typeString.contains("java.util.Date")) {
                                //System.out.println("valueobj = " + valueobj);
                                datestr = valueobj.toString();
                                date = parseDate(datestr);
                                fieldSets.put("attributes." + key + ".value", date);
                                //updateDocument.append("$set", new BasicDBObject().append("attributes." + key + ".value", date));
                            }
                        }

                        //update created_at and updated_at
                        String screated_at = null, supdated_at = null, sdeleted_at = null;
                        Date created_at, updated_at, deleted_at;

                        Object created = feat.get("created_at");
                        String typecreated = created == null ? "null" : created.getClass().toString();

                        Object updated = feat.get("updated_at");
                        String typeupdated = updated == null ? "null" : updated.getClass().toString();

                        Object deleted = feat.get("deleted_at");
                        String typedeleted = deleted == null ? "null" : deleted.getClass().toString();

                        if (created != null && !typecreated.contains("java.util.Date")) {
                            screated_at = created.toString();
                            created_at = parseDate(screated_at);
                            fieldSets.put("created_at", created_at);
                            //updateDocument.append("$set", new BasicDBObject().append("created_at", created_at));
                        }

                        if (updated != null && !typeupdated.contains("java.util.Date")) {
                            supdated_at = updated.toString();
                            updated_at = parseDate(supdated_at);
                            fieldSets.put("updated_at", updated_at);
                            //updateDocument.append("$set", new BasicDBObject().append("updated_at", updated_at));
                        }

                        if (deleted != null && !typedeleted.contains("java.util.Date")) {
                            sdeleted_at = deleted.toString();
                            deleted_at = parseDate(sdeleted_at);
                            fieldSets.put("deleted_at", deleted_at);
                            //updateDocument.append("$set", new BasicDBObject().append("deleted_at", deleted_at));
                        }
                        updateDocument.put("$set", fieldSets);

                        BasicDBObject searchQueryUpdate = new BasicDBObject().append("_id", new ObjectId(feature_id));

                        if (!fieldSets.isEmpty()) {
                            features.update(searchQueryUpdate, updateDocument);
                        }

                    }
                } else {
                    BasicDBObject updateDocument = new BasicDBObject();
                    BasicDBObject fieldSets = new BasicDBObject();
                    //update created_at and updated_at
                    String screated_at = null, supdated_at = null, sdeleted_at = null;
                    Date created_at, updated_at, deleted_at;

                    Object created = feat.get("created_at");
                        String typecreated = created == null ? "null" : created.getClass().toString();

                        Object updated = feat.get("updated_at");
                        String typeupdated = updated == null ? "null" : updated.getClass().toString();

                        Object deleted = feat.get("deleted_at");
                        String typedeleted = deleted == null ? "null" : deleted.getClass().toString();

                    if (created != null && !typecreated.contains("java.util.Date")) {
                        screated_at = created.toString();
                        created_at = parseDate(screated_at);
                        fieldSets.put("created_at", created_at);
                        //updateDocument.append("$set", new BasicDBObject().append("created_at", created_at));
                    }

                    if (updated != null && !typeupdated.contains("java.util.Date")) {
                        supdated_at = updated.toString();
                        updated_at = parseDate(supdated_at);
                        fieldSets.put("updated_at", updated_at);
                        //updateDocument.append("$set", new BasicDBObject().append("updated_at", updated_at));
                    }

                    if (deleted != null && !typedeleted.contains("java.util.Date")) {
                        sdeleted_at = deleted.toString();
                        deleted_at = parseDate(sdeleted_at);
                        fieldSets.put("deleted_at", deleted_at);
                        //updateDocument.append("$set", new BasicDBObject().append("deleted_at", deleted_at));
                    }
                    updateDocument.put("$set", fieldSets);

                    BasicDBObject searchQueryUpdate = new BasicDBObject().append("_id", new ObjectId(feature_id));

                    if (!fieldSets.isEmpty()) {
                        features.update(searchQueryUpdate, updateDocument);
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateCommonDates() {

    }

    /**
     * Date formats that can be recorded
     */
    private static final String[] formats = {
        "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd'T'HH:mm:ssZ",
        "yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
        "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "yyyy-MM-dd HH:mm:ss",
        "MM/dd/yyyy HH:mm:ss", "MM/dd/yyyy'T'HH:mm:ss.SSS'Z'",
        "MM/dd/yyyy'T'HH:mm:ss.SSSZ", "MM/dd/yyyy'T'HH:mm:ss.SSS",
        "MM/dd/yyyy'T'HH:mm:ss.SSSZ", "MM/dd/yyyy'T'HH:mm:ss",
        "MM/dd/yyyy'T'HH:mm:ssZ", "MM/dd/yyyy'T'HH:mm:ss",
        "yyyy:MM:dd HH:mm:ss", "yyyyMMdd",};

    /**
     * Parse string dates to date type
     *
     * @param d
     * @return
     */
    public static Date parseDate(String d) {
        if (d != null) {
            for (String parse : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(parse);
                try {
                    Date date = sdf.parse(d);
                    return date;
                } catch (ParseException e) {

                }
            }
        }
        return null;
    }

}
